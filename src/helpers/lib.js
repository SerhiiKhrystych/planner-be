/**
 * remove object properties from object
 * @param obj
 * @param exception: string | string[]
 */
export const getAllExcept = (obj, exception) => {
	if (!obj) return;

	const result = {};
	for (let key in obj) {
		if (typeof exception === 'string') {
			if (key !== exception) {
				console.log('key', key)
				console.log('exception', exception)
				result[key] = obj[key];
			}
		}
		if (exception instanceof Array) {
			if (!exception.includes(key)) result[key] = obj[key];
		}
	}

	return result;
};

/**
 * change property name
 * @param obj
 * @param currName
 * @param nextName
 * @returns {{}}
 */
export const changePropertyName = (obj, currName, nextName) => {
	if (!obj) return;
	if (typeof currName !== 'string' || typeof nextName !== 'string') return;
	const result = {};

	for (let key in obj) {
		if (key === currName) {
			result[nextName] = obj[key];
		} else {
			result[key] = obj[key];
		}
	}

	return result;
};

/**
 * return 'firstName', 'lastName', 'username', 'email' and _id as id
 * @param user
 */
export const prepareToSendUser = ({firstName, lastName, username, email, phoneNumber, _id}) => ({
	firstName,
	lastName,
	username,
	email,
	phoneNumber,
	id: _id
});

/**
 *
 * @param note
 */
export const prepareToSaveNote = ({title, description, priority, userId, createdAt, _id}) => ({
	title,
	description,
	priority,
	userId,
	createdAt,
	id: _id
});

export const prepareToSaveEvent = ({title, description, date, unreaded, userId, _id}) => ({
	title,
	description,
	date,
	unreaded,
	userId,
	id: _id,
});