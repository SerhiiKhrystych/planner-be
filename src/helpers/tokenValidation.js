import passport from 'passport';

export const commonTokenValidation = passport.authenticate('jwt', { session: false });