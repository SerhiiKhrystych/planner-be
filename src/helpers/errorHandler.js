import * as constants from './constants';

const errorHandler = (type, error, res) => {
	const errorTemplate = {success: false, error};
	switch (type) {
		case constants.REGISTER_ERROR:
			errorTemplate.msg = 'Error, when try to register new user.';
			res.status(500);
			break;
		case constants.GET_PROFILE_ERROR:
			errorTemplate.msg = 'Invalid username or email';
			res.status(500);
			break;
		case constants.AUTHENTICATE_ERROR:
			errorTemplate.msg = 'Invalid username or password';
			res.status(500);
			break;
		case constants.INVALID_TOKEN_ERROR:
			errorTemplate.msg = 'Invalid token';
			res.status(401);
			break;
		case constants.UPDATE_PROFILE_ERROR:
			errorTemplate.msg = 'Update user profile error';
			res.status(500);

		// case constants.CREATE_EMPLOYEE_ERROR:
		// 	errorTemplate.msg = 'Error, when try to create new employee';
		// 	res.status(500);
		// 	break;
		//
		// case constants.UPDATE_EMPLOYEE_ERROR:
		// 	errorTemplate.msg = 'Error, when try to update employee';
		// 	res.status(500);
		// 	break;
		//
		// case constants.GET_EMPLOYEE_BY_ID_ERROR:
		// 	errorTemplate.msg = 'Invalid employee id';
		// 	res.status(404);
		// 	break;
		//
		// case constants.GET_EMPLOYEES_LIST_ERROR:
		// 	errorTemplate.msg = 'Error, when try to get employees list';
		// 	res.status(404);
		// 	break;
		//
		// case constants.DELETE_EMPLOYEE_ERROR:
		// 	errorTemplate.msg = 'Error, when try to delete employee';
		// 	res.status(404);
		// 	break;

		default:
			console.log('looks like you forget to set constant');
			errorTemplate.msg = 'Oops.. something went wrong';
			res.status(500);
			break;


	}

	res.send(errorTemplate);
};

export default errorHandler;