import express from 'express';
import Router from 'express-promise-router';
import path from 'path';
import bodyparser from 'body-parser'
import cors from 'cors'
import passport from 'passport'
import mongoose from 'mongoose'
import socket from 'socket.io';

import users from './routes/users';
import events from './routes/events';
import notes from './routes/notes';

import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import dotenv from 'dotenv';
import fs from 'fs';

dotenv.config();

const envConfig = dotenv.parse(fs.readFileSync('.env.code.review'));
for (let k in envConfig) {
	process.env[k] = envConfig[k]
}

const app = express();
const router = Router();


mongoose.connect(process.env.DATABASE, { useNewUrlParser: true });

// On Connect
mongoose.connection.on('connected', () => {
	console.log('Connected to database', process.env.DATABASE)
});

// On Error
mongoose.connection.on('error', (error) => {
	console.log('Database error', error);
});

router.get('/', (req, res) => {
	res.send('Invalid Endpoint');
});

// Port Number

// CORS Middleware
app.use(cors());

// Set Static Folder
// app.use(express.static(path.join(__dirname, 'public')));

// Body Parser Middleware
app.use(bodyparser.json());

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());
passport.use(new JwtStrategy({
	jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt'),
	secretOrKey: process.env.SECRET
}, (token, done) => {
	return done(null, token);
}));

// Start Server
const server = app.listen(process.env.APP_PORT, () => {
	console.log('server started on port =', process.env.APP_PORT);
});
const io = socket(server);

app.use('/users', users);
app.use('/events', (req, res, next) => {
	req.io = io;
next();
}, events);
app.use('/notes', (req, res, next) => {
	req.io = io;
next();
}, notes);

// Index Route
app.use(router);
