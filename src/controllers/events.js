import socketioJwt from 'socketio-jwt';
import errorHandler from '../helpers/errorHandler';
import * as constants from '../helpers/constants';
import { prepareToSaveEvent } from '../helpers/lib';
import Event from '../models/events';

export async function getEvents (req, res, next) {
	try {
		const events = await Event.getEvents(req.user.id);
		res.json({ success: true, events: events.map(event => prepareToSaveEvent(event)) });
	} catch (error) {
		console.log('get events error');
	}
}

export async function getEvent (req, res, next) {
	try {
		const event = await Event.getEvent(req.params.id, req.user.id);

		res.json({ success: true, event });
	} catch (error) {

	}
}

export async function createEvent (req, res, next) {
	try {
		const event = await new Event(Object.assign({}, req.body, { userId: req.user.id })).save();
		req.io.use(socketioJwt.authorize({
			secret: process.env.SECRET,
		})).emit('push', { type: 'events',payload: 'Event Created' });
		res.json({ success: true, event: prepareToSaveEvent(event) });
	} catch (error) {
		console.log('error', error);

	}
}

export async function updateEvent (req, res, next) {
	try {
		await Event.updateEvent(req.body.id, req.user.id, req.body);
		const event = await Event.getEvent(req.body.id, req.user.id);
		req.io.use(socketioJwt.authorize({
			secret: process.env.SECRET,
		})).emit('push', { type: 'events', payload: 'Event Updated' });
		res.json({ success: true, event: prepareToSaveEvent(event) });
	} catch (error) {
		console.log('updateEvent error', error);
	}
}

export async function deleteEvent (req, res, next) {
	try {
		await Event.deleteEvent(req.body.id, req.user.id);
		res.json({ success: true });
	} catch (error) {
		console.log('deleteEvent error', error);
	}
}