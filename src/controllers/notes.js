import socketioJwt from 'socketio-jwt';
import errorHandler from '../helpers/errorHandler';
import * as constants from '../helpers/constants';
import { prepareToSaveNote } from '../helpers/lib';
import Note from '../models/notes';

export async function getNotes (req, res, next) {
	try {
		const notes = await Note.getNotes(req.user.id);
		res.json({ success: true, notes: notes.map(note => prepareToSaveNote(note)) });
	} catch (error) {
		console.log('get notes error');
	}
}

export async function getNote (req, res, next) {
	try {
		const note = await Note.getNote(req.params.id, req.user.id);
		res.json({ success: true, note });
	} catch (error) {

	}
}

export async function createNote (req, res, next) {
	try {

		const note = await new Note(Object.assign({}, req.body, { userId: req.user.id })).save();
		req.io.use(socketioJwt.authorize({
			secret: process.env.SECRET
		})).emit('push', { type: 'notes', payload: 'Note Created' });
		res.json({ success: true, note: prepareToSaveNote(note) });
	} catch (error) {
		errorHandler(constants.CREATE_NOTE_ERROR, error, res);
	}
}

export async function updateNote (req, res, next) {
	try {
		await Note.updateNote(req.body.id, req.user.id, req.body);
		const note = await Note.getNote(req.body.id, req.user.id);
		req.io.use(socketioJwt.authorize({
			secret: process.env.SECRET,
		})).emit('push', { type: 'notes', payload: 'Note Updated' });
		res.json({ success: true, note: prepareToSaveNote(note) });
	} catch (error) {
		errorHandler(constants.UPDATE_NOTE_ERROR, error, res);
	}
}

export async function deleteNote (req, res, next) {
	try {
		const response = await Note.deleteNote(req.params.id, req.user.id);

		res.json({ success: true });
	} catch (error) {

	}
}