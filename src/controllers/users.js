import errorHandler from '../helpers/errorHandler';
import * as constants from '../helpers/constants';
// import config from '../config';

import { prepareToSendUser } from '../helpers/lib';

import User from '../models/user';

import jwt from 'jsonwebtoken';

export async function register (req, res, next) {
	try {
		await User.init(); // todo: find out
		const user = await new User(req.body).save();
		res.json({ success: true, msg: 'User Created', user: prepareToSendUser(user) });
		// next();
	} catch (error) {
		errorHandler(constants.REGISTER_ERROR, error, res);
	}
}

export async function login (req, res, next) {
	const { username, password } = req.body;
	try {
		const user = await User.getUserByUsername(username);
		const passwordCompared = await user.comparePassword(password);
		if (passwordCompared) {
			const token = jwt.sign(prepareToSendUser(user), process.env.SECRET, {
				expiresIn: 604800 // 1 week
			});

			res.json({
				success: true,
				token: `JWT ${token}`,
				user: prepareToSendUser(user)
			});
			// next();
		} else {
			errorHandler(constants.AUTHENTICATE_ERROR, null, res);
		}
	} catch (error) {
		errorHandler(constants.AUTHENTICATE_ERROR, error, res);
	}
}

export async function getProfile (req, res, next) {
	const { username } = req.user;
	if (!username) {
		errorHandler(constants.INVALID_TOKEN_ERROR, null, res);
	} else {
		try {
			const user = await User.getUserByUsername(username);
			res.json({ success: true, user: prepareToSendUser(user) });
		} catch (error) {
			errorHandler(constants.GET_PROFILE_ERROR, error, res);
		}
	}
}

export async function validateToken (req, res, next) {
	const { user } = req;
	if (!!user) {
		res.json({ success: true });
	} else {
		errorHandler(constants.GET_PROFILE_ERROR, null, res);
	}
}

export async function sendRestorePasswordEmail (req, res, next) {
	const { email } = req;
	// todo: create token and send it on user email
}

export async function changePassword (req, res, next) {
	const { password, id, token } = req;
	// todo: check token;
	// if token valid, update user password
	// else return error
}

export async function updateProfile(req, res, next) {
	const { user } = req;
	try {
		await User.updateUser(user.id, req.body);
		res.json({
			success: true,
			user: req.body
		});
	} catch (e) {
		errorHandler(constants.UPDATE_PROFILE_ERROR, null, res);
	}
}