import Router from 'express-promise-router';

import { getEvents, getEvent, createEvent, updateEvent, deleteEvent } from '../controllers/events';

import { commonTokenValidation } from '../helpers/tokenValidation';

const router = Router();

router.get('/', commonTokenValidation, getEvents);
router.get('/:id', commonTokenValidation, getEvent);
router.post('/', commonTokenValidation, createEvent);
router.put('/', commonTokenValidation, updateEvent);
router.delete('/:id', commonTokenValidation, deleteEvent);

export default router;