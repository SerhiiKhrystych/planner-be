import Router from 'express-promise-router';

import { getNotes, getNote, createNote, updateNote, deleteNote } from '../controllers/notes';

import { commonTokenValidation } from '../helpers/tokenValidation';

const router = Router();

router.get('/', commonTokenValidation, getNotes);
router.get('/:id', commonTokenValidation, getNote);
router.post('/', commonTokenValidation, createNote);
router.put('/', commonTokenValidation, updateNote);
router.delete('/:id', commonTokenValidation, deleteNote);

export default router;