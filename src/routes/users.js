import Router from 'express-promise-router';

import { register, login, getProfile, sendRestorePasswordEmail, changePassword, validateToken, updateProfile } from '../controllers/users';

import { commonTokenValidation } from '../helpers/tokenValidation';

const router = Router();

// Register
router.post('/register', register);

// Authenticate
router.post('/login', login);

// Profile
router.get('/profile', commonTokenValidation, getProfile);

// Update Profile
router.put('/profile', commonTokenValidation, updateProfile);

// Restore password
router.get('/restore-password', sendRestorePasswordEmail);

// Change password
router.post('/change-password', changePassword);

// Validate
router.get('/validate', commonTokenValidation, validateToken);

export default router;