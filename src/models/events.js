import mongoose, { Model } from 'mongoose';

class Event extends mongoose.Schema {
	constructor() {
		const event = super({
			title: {
				type: String,
				required: true
			},
			description: {
				type: String,
				required: true
			},
			date: {
				type: Date,
				required: true
			},
			unreaded: {
				type: Boolean,
				required: true,
				default: true
			},
			userId: {
				type: String,
				required: true
			}
		});

		event.pre('save', function (next) {
			console.log('Event pre save');
			// this.date = new Date();
			next();
		});

		event.statics = {
			getEvents(userId) {
				return this.find({ userId });
			},
			getEvent(id, userId) {
				return this.findOne({ _id: id, userId });
			},
			updateEvent(id, userId, event) {
				return this.updateOne({ _id: id, userId }, event);
			},
			deleteEvent(id, userId) {
				return this.deleteOne({ _id: id, userId });
			}
		};

	}
}

export default mongoose.model('Event', new Event);