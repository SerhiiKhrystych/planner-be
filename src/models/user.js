import * as bcrypt from 'bcryptjs'
import mongoose, { Model } from 'mongoose';

class User extends mongoose.Schema {
	constructor() {
		const user = super({
			firstName: {
				type: String,
				required: true
			},
			lastName: {
				type: String,
				required: true
			},
			email: {
				type: String,
				required: true,
				unique: true
			},
			username: {
				type: String,
				required: true
			},
			phoneNumber: {
				type: String,
				required: true
			},
			password: {
				type: String,
				required: true,
			}
		}, {
			versionKey: false
		});

		user.pre('save', async function (next) {
			console.log('pre save user')
			const salt = await bcrypt.genSalt(10);
			const hash = await bcrypt.hash(this.password, salt);
			this.password = hash;
			next();
		});

		user.methods = {
			async comparePassword(password) {
				console.log('this.password', this.password)
				return bcrypt.compare(password, this.password);
			}
		};

		user.statics = {
			saveUser(user) {
				return this.save(user);
			},
			getUserById(id) {
				return this.findById(id);
			},
			getUserByUsername(username) {
				return this.findOne({ username });
			},
			updateUser(id, user) {
				return this.updateOne({ _id: id }, user);
			},
			changePassword(user) {
				const { id: _id, password } = user;
				return this.updateOne({ _id }, password);
			}
		}
	}
}

export default mongoose.model('User', new User);