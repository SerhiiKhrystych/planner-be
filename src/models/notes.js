import mongoose, { Model } from 'mongoose';

class Note extends mongoose.Schema {
	constructor() {
		const note = super({
			title: {
				type: String,
				required: true
			},
			description: {
				type: String,
				required: true
			},
			createdAt: {
				type: Date,
				required: true,
				default: Date.now
			},
			priority: {
				type: String,
				enum: ['Low', 'Middle', 'High']
			},
			userId: {
				type: String,
				required: true
			}
		}, {
			versionKey: false
		});

		note.pre('save', async function (next) {
			console.log('NOTE pre save');
			// this.createdAt = new Date();
			// this.dateId = `${this.createdAt.getMonth()}-${this.createdAt.getDate()}-${this.createdAt.getFullYear()}`
			next();
		});

		note.statics = {
			getNotes(userId) {
				return this.find({ userId });
			},
			getNote(id, userId) {
				return this.findOne({ _id: id, userId });
			},
			updateNote(id, userId, note) {
				return this.updateOne({ _id: id, userId }, note);
			},
			deleteNote(id, userId) {
				return this.deleteOne({ _id: id, userId })
			}
		}
	}
}

export default mongoose.model('Note', new Note);
